export const GRAPHQL_PORT = 4000;
export const MONGODB_DATABASE_NAME = 'eventchecking';
export const MONGODB_CONNECTION_URI = `mongodb://localhost:27017/${MONGODB_DATABASE_NAME}`;
export const SECRET = 'supersecret';