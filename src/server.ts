import * as mongoose from 'mongoose';
import * as express from 'express';
import { buildSchema } from 'type-graphql';
import { ApolloServer } from "apollo-server-express";
import * as bodyParser from 'body-parser'
import { Container } from 'typedi';
import * as jwt from 'jsonwebtoken'

import * as config from '../config';
import { authChecker } from './authorization/authChecker';
import { User } from './types/user';

interface Context {
    user: User;
}

async function bootstrap() {
    await mongoose.connect(config.MONGODB_CONNECTION_URI, {}, () => console.log(`Connected to MongoDB`));

    const app = express();

    const schema = await buildSchema({
        resolvers: [__dirname + '/resolvers/**/*.ts'],
        container: Container,
        authChecker
    })

    const server = new ApolloServer({
        schema,
        context: ({req}): Context => {
            let user = undefined;
            let token: any = req.headers.authorization || '';

            if (token.startsWith('Bearer ')) token = token.slice(7, token.length);

            jwt.verify(token, config.SECRET, (err, decoded) => {
                if (!err) user = decoded;
            });

            return { user }
        },
        formatError: (err) => {
            return {
                message: err.message,
                path: err.path,
                extensions: err.extensions? err.extensions.code : undefined
            }
        }
    });

    app.use(bodyParser.json());

    server.applyMiddleware({app})

    app.listen(config.GRAPHQL_PORT, () => console.log(`Server listening on port ${config.GRAPHQL_PORT}`));
}

bootstrap();