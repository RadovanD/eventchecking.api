import { Resolver, Mutation, Arg } from "type-graphql";
import { User } from "../types/user";
import { UserService } from "../services/userService";

@Resolver(of => User)
export class UserResolver {
    constructor(private userService: UserService) { }

    @Mutation(returns => String)
    async login(@Arg("username") username: string, @Arg("password") password: string) {
        return await this.userService.login(username, password);
    }
}