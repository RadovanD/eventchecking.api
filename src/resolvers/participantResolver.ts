import { Resolver, Query, Authorized, Mutation, Arg } from "type-graphql";

import { Participant } from "../types/participant";
import { ParticipantService } from "../services/participantService";

@Resolver(of => Participant)
export class ParticipantResolver {
    constructor(private participantService: ParticipantService) { }

    @Authorized()
    @Query(returns => [Participant])
    async participants() {
        return await this.participantService.findAll();
    }

    @Authorized()
    @Mutation(returns => Participant)
    async addParticipant(@Arg('firstName') firstName: string, @Arg('lastName') lastName: string) {
        return await this.participantService.create({
            firstName,
            lastName
        });
    }

    @Authorized()
    @Mutation(returns => Participant)
    async checkIn(@Arg('id') id: string) {
        return await this.participantService.checkIn(id);
    }

    @Authorized()
    @Mutation(returns => Participant)
    async checkOut(@Arg('id') id: string) {
        return await this.participantService.checkOut(id);
    }
}