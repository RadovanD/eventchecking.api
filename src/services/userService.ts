import { Service } from 'typedi';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import * as config from '../../config';
import { UserModel } from '../types/user';

@Service()
export class UserService {
    async login(username: string, password: string) {
        const user = await UserModel.findOne({ username })
        if (!user) throw new Error('No user with that username')

        const valid = await bcrypt.compare(password, user.password)
        if (!valid) throw new Error('Incorrect password')

        return jwt.sign(
            { id: user.id, username: user.username },
            config.SECRET,
            { expiresIn: '1d' }
        )
    }
}
