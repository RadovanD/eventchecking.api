import { Service } from 'typedi';
import { ParticipantModel } from "../types/participant";

@Service()
export class ParticipantService {
    findAll() {
        return ParticipantModel.find({});
    }

    create(data: any) {
        return ParticipantModel.create(data);
    }

    async checkIn(id: string) {
        const participant = await ParticipantModel.findById(id);

        participant.checkedIn = true;

        await participant.save();

        return participant;
    }

    async checkOut(id: string) {
        const participant = await ParticipantModel.findById(id);

        participant.checkedIn = false;

        await participant.save();

        return participant;
    }
}
