import { AuthChecker } from 'type-graphql';

interface AuthContext {
    user?: {
        username: string,
        password?: string
    };
}

export const authChecker: AuthChecker<AuthContext> = ({ context: { user } }) => user !== undefined