import { UserModel } from "../types/user";
import * as mongoose from 'mongoose';
import * as config from '../../config';
import * as bcrypt from 'bcrypt';

const users = [
    {
        username: 'admin',
        password: 'admin'
    }
];

async function seed() {
    await mongoose.connect(config.MONGODB_CONNECTION_URI)
    .then(async _ => {
        for(const user of users) {
            const password = await bcrypt.hash(user.password, 10);
            await UserModel.create({
                username: user.username,
                password
            });
            console.log('Created user');
        }
    })
}

seed()
.then(_ => console.log('Done'))
.then(_ => process.exit())
.catch(err => console.error(err));