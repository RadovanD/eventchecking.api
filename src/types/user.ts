import { ObjectType, Field } from "type-graphql";
import { prop as Property, Typegoose } from "typegoose";
import { ObjectId } from 'mongodb';

@ObjectType()
export class User extends Typegoose{
  @Field(type => String)
  readonly _id: ObjectId;

  @Field()
  @Property({ required: true })
  username: string;

  @Field()
  @Property({ required: true })
  password: string;

  @Field()
  @Property({ default: new Date() })
  createdAt: Date;

  @Field()
  @Property({ default: new Date() })
  updatedAt: Date;
}

export const UserModel = new User().getModelForClass(User);