import { ObjectType, Field, Float } from "type-graphql";
import { prop as Property, Typegoose } from "typegoose";
import { ObjectId } from 'mongodb';

@ObjectType()
export class Participant extends Typegoose{
  @Field(type => String)
  readonly _id: ObjectId;

  @Field()
  @Property({ required: true })
  firstName: string;

  @Field()
  @Property({ required: true })
  lastName: string;

  @Field(type => Boolean)
  @Property({ default: false })
  checkedIn: boolean;
}

export const ParticipantModel = new Participant().getModelForClass(Participant);